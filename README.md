# IBM i Chroot (version 2)
:rotating_light: This project has moved to GitHub. The new repository is [here](https://github.com/IBM/ibmichroot). :rotating_light:

